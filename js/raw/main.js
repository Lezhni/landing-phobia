$ = jQuery;

$(document).on('ready', ready);
$(document).on('scroll', scroll);
$(window).on('resize', resize);

// $('.popup').click(function() {
//
//     var target = $(this).attr('href');
//     $.magnificPopup.open({
//         items: {
//             src: target,
//             type: 'inline'
//         }
//     });
//
//     return false;
// });

$('.scroll-btn').click(function() {

    var block = $(this).attr('href');
    if (block == '#')
        return false;

    if ($(window).width() > 1000) {
        var header = 130;
    } else {
        var header = 0;
    }

    var path = $(block).offset().top - header;
    $('body, html').animate({
        scrollTop: path
    }, 800);

    return false;
});

// $('form').submit(function() {
//
//     var form = $(this);
//     var formData = form.serialize();
//
//     form.find('input[type=submit]').attr('disabled', 'disabled').val('Отправляем...');
//
//     if (formData) {
//         $.post('send.php', formData, function(data) {
//             if (data == 'sended') {
//                 $.magnificPopup.open({
//                     items: {
//                         src: '#thanks',
//                         type: 'inline'
//                     }
//                 });
//             } else {
//                 console.log(data);
//             }
//         });
//     }
//
//     form.find('input[type=submit]').removeAttr('disabled').val('Отправить');
//
//     return false;
// });

function ready() {

    $('.tooltip').tooltipster({
        contentAsHTML: true,
        position: 'bottom',
        theme: 'tooltipster-default item-tooltip'
    });
}

// #scrollspy
$('.animate').on('scrollSpy:enter', function() {

   if (!$(this).hasClass('visible')) {
       $(this).addClass('visible');
   }
});

$('.winner-selection-steps').on('scrollSpy:enter', function() {
        var speed = 400;
        if (!$(this).hasClass('visible')) {
            $(this).addClass('visible');
            $(".winner-selection-step").eq(0).stop().animate({'opacity': 1}, speed).addClass('visible');
            $(".winner-selection-arrow").eq(0).stop().delay(speed*0.5).animate({'opacity': 1}, speed).addClass('visible');
            $(".winner-selection-step").eq(1).stop().delay(speed).animate({'opacity': 1}, speed).addClass('visible');
            $(".winner-selection-arrow").eq(1).stop().delay(speed*2).animate({'opacity': 1}, speed).addClass('visible');
            $(".winner-selection-step").eq(2).stop().delay(speed*3).animate({'opacity': 1}, speed).addClass('visible');
        }
    });

$('.animate, .winner-selection-steps').scrollSpy();


function scroll() {}

function resize() {}
